param([string]$name,[string]$hostname,[string]$destination)

$properties="sftpshare.properties"
$regex = "[^\=]+$"

## functions

## SetUpBatch
## sets up the batch for this user

function SetUpBatch{
	param([string]$hostname,[string]$directory,[int]$AddDeleteBatch)
	
	$files = Get-ChildItem $directory -Attributes !Directory+!System
	foreach ($f in $files){
		$content = $f.FullName
		"put `""+$content +"`""| Add-Content -Path 'tmp_batch'
		if($AddDeleteBatch -eq 1){
			"del `""+$content +"`""| Add-Content -Path 'del_script.bat'
		}
	}
}

## script bulk

#create start of batch file
"Transfering files for friend "+$name+"..."

"cd " + $destination | Set-Content -Path 'tmp_batch'
"" | Set-Content -Path 'del_script.bat'

$filesout = Get-Content -Path $properties | Where { $_ -match "sftpshare\.files\.out\.staging\.directory=(.*)" }
$filesout =[regex]::matches($filesout, $regex).value

# Always try to send files in the global directory. 
# Do not add the files to the delete batch yet, we'll do that after all
# the users are processed.
SetUpBatch $hostname $filesout 0

# Try to send files in your friend's directory.
# You want to clear out this directory when you're done btw.

$filesout = $filesout + "\" +$name
SetUpBatch $hostname $filesout 1

#executes the batch
sftp -b "tmp_batch" $hostname

./del_script.bat

# delete the temp file for the batch
Remove-Item -Path 'tmp_batch'
Remove-Item -Path 'del_script.bat'


