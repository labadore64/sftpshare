param([string]$name)

if((Get-Content "friendlist") -ne $Null){

	$properties="sftpshare.properties"

	while([string]::IsNullOrEmpty($name)){
		$name = Read-Host "Which friend do you want to remove?"
		if([string]::IsNullOrEmpty($name)){
			"You must input your friend's name!"
			""
			continue
		}
		
		$testname = Get-Content -Path "friendlist" | Where { $_ -match "("+$name+")" }
		if([string]::IsNullOrEmpty($testname)){
			"This friend isn't in your friend list!"
			""
			$name=""
			continue
		}
	}

	## remove the friend's name

	$friends = Get-Content -Path "friendlist" | Where { $_ }

	$friendsArray = $friends.Split("{`r`n}")

	for ($i=0; $i -lt $friendsArray.Count-2; $i+=3) {
		if (!$friendsArray[$i].StartsWith($name)){
			$newfile+=$friendsArray[$i] + "`r`n"
			$newfile+=$friendsArray[$i+1] + "`r`n"
			$newfile+=$friendsArray[$i+2] + "`r`n"
			$newfile+="`r`n"
		}
	}

	$newfile | Set-Content -Path 'friendlist'

	## removes your friend's transfer directory if it exists

	$regex = "[^\=]+$"
	$filesout = Get-Content -Path $properties | Where { $_ -match "sftpshare\.files\.out\.staging\.directory=(.*)" }
	$filesout =[regex]::matches($filesout, $regex).value
	$filesout = $filesout + "\" +$name

	if(Test-Path $filesout -PathType Container){
		""
		"Deleting " + $name +"'s sharing directory."
		Remove-Item $filesout -recurse -force
		""
	} else {
		""
		$name+"'s sharing directory was already deleted."
		""
	}

	"Successfully removed " +$name +" from the friend list."
} else {
	"Your friend list is already empty."
}