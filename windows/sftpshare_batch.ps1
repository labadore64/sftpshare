## TestSFTP

## Tests the SFTP connection. if it fails it doesn't even try to transfer.

function TestSFTP(){
	param([string]$hostname)
	# do the test

	"exit" | Set-Content -Path 'tmp_batch'
	$TEXT = & sftp -b "tmp_batch" $hostname 2>&1
	Remove-Item -Path 'tmp_batch'

	## test for errors

	if($TEXT -like '*Connection timed out*') {
		return 1;
	} 
	if($TEXT -like '*Connection closed*') {
		return 1;
	}
	if($TEXT -like '*No route to host*') {
		return 1;
	}

	## if there are no errors return 0

	return 0;
}

$friends = Get-Content -Path "friendlist" | Where { $_ }

$friendsArray = $friends.Split("{`r`n}")

for ($i=0; $i -lt $friendsArray.Count-2; $i+=3) {
	if (!$friendsArray[$i].StartsWith("#") -And !$friendsArray[$i+1].StartsWith("#") -And !$friendsArray[$i+2].StartsWith("#")){
		$char=$friendsArray[$i+1]+"="+$friendsArray[$i+2]
		# get the username
		$name=$friendsArray[$i]
		
		# get the user's hostname
		$regex = "^[^=]+"
		$userhostname=[regex]::matches($char, $regex).value
		
		# get the destination receiving directory.
		
		$regex = "[^\=]+$"
		$destination=[regex]::matches($char, $regex).value	
		
		# test the connection. if valid, continue.
		"Testing connection for friend "+$name+"..."
		
		$result = TestSFTP $userhostname

		if($result -eq 0){
			"Connection succeeded."
			.\sftpshare.ps1 $name $userhostname $destination
		} else {
			"Connection unsuccessful. Skipping "+$name+"..."
		}
	}
}