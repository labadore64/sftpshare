param([string]$name,[string]$hostname,[string]$directory)

$properties="sftpshare.properties"

while([string]::IsNullOrEmpty($name)){
	$name = Read-Host "What is your friend's name?"
	if([string]::IsNullOrEmpty($name)){
		"You must input your friend's name!"
		""
		continue
	}
	
	$testname = Get-Content -Path "friendlist" | Where { $_ -match "("+$name+")" }
	if(![string]::IsNullOrEmpty($testname)){
		"This friend already exists!"
		""
		$name=""
		continue
	}
}
""

while([string]::IsNullOrEmpty($hostname)){
	$hostname = Read-Host "What is your friend's connection string?"
	if([string]::IsNullOrEmpty($hostname)){
		"You must put in your friend's connection string!"
		""
		continue
	}
	
	$testname = Get-Content -Path "friendlist" | Where { $_ -match "("+$hostname+")" }
	if(![string]::IsNullOrEmpty($testname)){
		"This connection string already exists for someone else!"
		""
		$hostname=""
		continue
	}
}
""
while([string]::IsNullOrEmpty($directory)){
	$directory = Read-Host "What is the directory on your friend's computer where the files should be transfered?"
	if([string]::IsNullOrEmpty($hostname)){
		"You must put in your friend's receiving directory!"
		""
	}
}

$name | Add-Content -Path 'friendlist'
$hostname | Add-Content -Path 'friendlist'
$directory | Add-Content -Path 'friendlist'
"" | Add-Content -Path 'friendlist'

## adds your friend's transfer directory if it doesn't already exist

$regex = "[^\=]+$"
$filesout = Get-Content -Path $properties | Where { $_ -match "sftpshare\.files\.out\.staging\.directory=(.*)" }
$filesout =[regex]::matches($filesout, $regex).value
$filesout = $filesout + "\" +$name

if(Test-Path $filesout -PathType Container){
	""
	"Path for this friend already exists. No need to create it."
	""
} else {
	""
	"Creating path for "+$name+": " + $filesout
	New-Item $filesout -ItemType Directory
	""
}

"Congratulations! You added your friend to the transfer list."
"Please keep in mind!"
"You must exchange SSH tokens with your friend in order for the script to function automatically."
""