Friend List Management
======================

You can manage friends from the command line by running various scripts.

Adding a friend
---------------

You can add friends by running ``add_friend.bat`` (Windows) or ``add_friend.sh`` (UNIX). Follow the prompts to add your friend to the list.

```
What is your friend's name?: joe

What is your friend's host address?
It should be in this format:
[username]@[hostname] [options]: sftpshare@joescoolcomputer.com

What is the directory on your friend's computer where the files should be transfered?: /home/joe/receiving

Creating path for joe: C:\sftpshare\sending\joe

Congratulations! You added your friend to the transfer list.
Please keep in mind!
You must exchange SSH tokens with your friend in order for the script to function automatically.
```
A few things to keep in mind:
* For Windows receiving directories, you must put them in this format ``/[drive letter]:/[path]`` or else it will not work. Example: ``/C:/sftpshare/receiving``
* You can use the sftp connection string for the host address instead.
* In order for the script to be automated, the friend must have exchanged SSH keys with you.

Removing a friend
-----------------

To remove a friend, run either ``remove_friend.bat`` (Windows) or ``remove_friend.sh`` (UNIX) and fill out the prompts:

```
Which friend do you want to remove?: joe

Deleting joe's sharing directory.

Successfully removed joe from the friend list.
```

Manual Modification
-------------------

All friends are stored in the ``friendlist`` file. Friend entries have 3 lines each. The first line is the friend's name as known in your sending directory. The second line is the SFTP connection address, including arguments. The third line is the directory where to place files being sent to this friend. All empty lines are ignored, but there should be at least one empty line at the end of a file.

For example:
```
joe
sftpshare@joescoolcomputer.com
/home/joe/receiving

sarah
sftpshare@sarahsradmachine.net
/C:/sftpshare/receiving

```