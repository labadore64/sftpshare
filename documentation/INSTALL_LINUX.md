Linux Installation Instructions
==============================

Prerequisites
------------

- openSSH (usually already installed) or another type of SSH server
- A friend to share files with, that you have exchanged SSH keys with
- A service user account (not necessary, but highly recommended for security purposes). For the sake of this manual, the user account is named ``labadore64`` and the service account is named ``sftpshare``.

Main Installation
-----------------

You can install it to any directory you want, but for this example I used ``/home/sftpshare/sbin``. Assuming you have downloaded the entire package and are in the base ``/sftpshare`` directory:

```
cp ./unix/* /home/sftpshare/sbin
cd /home/sftpshare/sbin
```

Next, select a directory for placing files to be sent. You will drop off these files in this directory so they can be sent by the script. Let's assume that it will be ``/home/labadore64/sending``. Edit the line in sftpshare.properties to reflect this.
```
# the staging directory for modified files in the sending directory.
sftpshare.files.out.staging.directory=/home/labadore64/sending
```

Make sure that:
- The service account can read, write and delete files in the sending directory.
- The service account can execute the scripts.

You can then add the batch script ``sftpshare_batch.sh`` to a schedule, such as crontab:
```
30 * * * * /home/sftpshare/sbin/sftpshare_batch.sh >/dev/null 2>&1
```

Optional Installation
---------------------

Since you are opening your computer to SSH connections, you should probably have some restrictions on who can just log in, as well as update other configurations such as the listening port number. This is by no means a comprehensive setup and assumes you're using openSSH. This requires you modifying your ``sshd_config`` file, which is located usually at ``/etc/ssh/sshd_config``.

An example of a restriction: Using ``AllowUsers`` to only allow SSH access to the service account:
```
AllowUsers sftpshare
```

You can read more about ``sshd_config`` [here](https://www.ssh.com/ssh/sshd_config/) and [here](https://man.openbsd.org/sshd_config).