Scripts
=======

add_friend.ps1 / add_friend.sh
--------------
Powershell script is called by batch file ``add_friend.bat``. 

Adds a friend to the friend list. Arguments must be provided in the following order: Name, Connection String, Receiving Directory. However, you can omit some arguments. It will ask for the missing arguments when running the script.

If you want to add a user without any prompts:

Windows:
```
add_friend.bat sarah sftpshare@sarahsradcomputer.com /C:/sftpshare/receiving
```

Unix:
```
./add_friend.sh joe sftpshare@joescoolcomputer.com /home/joe/receiving
```

remove_friend.ps1 / remove_friend.sh
-----------------
Powershell script is called by batch file ``remove_friend.bat``.

Removes a friend from the friend list. An argument can be provided for the name, otherwise the script will prompt for the name. Will not do anything if the ``friendlist`` file is empty.

If you want to remove a user without any prompts:

Windows:
```
remove_friend.bat sarah
```

Unix:
```
./remove_friend.sh joe
```

sftpshare_batch.ps1 / sftpshare_batch.sh
-------------------
Powershell script is called by batch file ``sftpshare.bat``.

This script iterates through the ``friendlist`` file. For each friend entry, it runs a sftp test. If it connects successfully, it executes ``sftpshare.ps1``/``sftpshare.sh`` with the name, connection string and receiving directory parameters being passed.

If you want to execute it without a schedule, just call the script.

Windows:
```
sftpshare.bat
```

Unix:
```
sftpshare_batch.sh
```

sftpshare.ps1 / sftpshare.sh
-------------
Powershell script is called by script ``sftpshare_batch.ps1``.

This script is used by ``sftpshare_batch.ps1``/``sftpshare_batch.sh`` to handle the processing of one friend in your friendlist. It takes 3 arguments - name, connection string and receiving directory. It will build a batch file for executing the file transfer - it will ``cd`` into the receiving directory, and ``put`` each file individually. It will send all files in the root file sending directory, as well as all the files in the friend's sending directory. Files in the friend's sending directory will be deleted after being sent.

Misc. Files
===========

friendlist
-----------

A list of friends and their connection settings. Each entry is in this format:

```
[name]
[connection string]
[receiving directory]

```

sftpshare.properties
--------------------

Properties file, currently only has location of sending directory locally.

```
# the staging directory for modified files in the sending directory.
sftpshare.files.out.staging.directory=[files out directory]
```
