File Management
===============

Once you have set up ``sftpshare.properties``, you can place files in the designated directory and whenever ``sftpshare.bat`` (Windows) or ``sftpshare_batch.sh`` (UNIX) is run, it will transfer the files in those directories.

When you create new friends, it will create a new directory for them. Every file placed in this directory will only be sent to that specific friend. Note that these names are the names that you give the friend, not the username used to log into SFTP. Any file placed in the root sending directory will be sent to all of your friends - think of these as broadcast files. So for example:

```
# All these files will be transfered to all friends
.\testfile.png 
.\cool_story.txt

# All these files will only be transfered to "joe"
.\joe\birds.png
.\joe\poem.txt

# All of these files will only be transfered to "sarah"
.\sarah\cats.png
.\sarah\index.html
```
