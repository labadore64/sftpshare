Windows Install Instructions
==============================

Prerequisites
------------

- Windows 10
- Admin Privileges
- Powershell
- A friend to share files with, that you have exchanged SSH keys with
- A service user account, if you're using Windows Pro. For the sake of this manual, the user account is named ``labadore64`` and the service account is named ``sftpshare``.

Installing openSSH Client and openSSH Server
---------------------------------

Windows doesn't natively support SSH due to many design restrictions, but recent editions do support an SSH client and server that helps manage SSH connections like UNIX systems do; however, they require a bit of set up to get working. It is useful to have another computer on the local network to test with, or possibly a friend over the internet.

You can also reference [Microsoft's official documentation](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_overview).

First, install both openSSH Client and openSSH Server on your Windows machine.

First, go to Apps. Then, select "See optional features".

![Optional Features is right below the title Apps and Features and before the App execution aliases link.](img/windows_install_ssh_01.png)

Then, select "Add a Feature".

![Add a feature is below See Optional feature history and has a big plus sign next to it.](img/windows_install_ssh_02.png)

A list of possible optional features will appear. Scroll down to "OpenSSH Client" and click on it. "Select Install". You may already have the Client installed; if this is the case, just install the server.

![Select OpenSSH Client, which will reveal the install button. Press it.](img/windows_install_ssh_03.png)

After adding the client, it will install. When you are done, restart your system, and do the same with the OpenSSH Server.

![Displays installation progress.](img/windows_install_ssh_04.png)

Once you have completed installing both, make sure the system has been restarted. 

You will know if it is working, if you can SSH into another computer from the command line, like this. This command should always fail, but as long as it recognizes the ssh command, the client is working.

```
ssh username@bogusname
```

Configuring OpenSSH Server
--------------------------

In order to receive files from other friends, you must have the OpenSSH Server configured. Follow the instructions [on Microsoft's official documentation](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse) to set up the server. Once you have it working, you should be able to log in as a user if the server service is running.

When you first initially install the service, the server and the SSH key agent will not be running on startup. You will need to configure both to start automatically. To do this, type "services" in the start menu, and press enter. Find OpenSSH SSH Server and Authentication Agent in the list.

![A list of services running, with OpenSSH SSH Server and Authetication Agent visible](img/windows_install_ssh_05.png)

Right click, select properties, and change the start up type to automatic. Also, start the service if necessary.

![A list of services running, with OpenSSH SSH Server and Authetication Agent visible](img/windows_install_ssh_06.png)

Configuring SSH Keys
--------------------

To authenticate without logging in, you will need to exchange SSH keys with your friends. For those using Windows, you will have to work with the OpenSSH Authentication Agent in order to update your keys. You can follow [Microsoft's official documentation](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement) here, although if it doesn't seem to work, you can try this solution from [Stack Overflow](https://stackoverflow.com/a/50502015):

After creating the ``authorized_keys`` file by following Microsoft's instructions, right click on ``authorized_keys`` file, and go to ``Properties -> Security -> Advanced``. Click "Disable Inheritance", and select the "Convert inherited permissions into explicit permissions on this object". Finally, delete all the permissions that are not 1) the SFTP user and 2) ``SYSTEM``. If still not able to connect from another machine with the key, you may also need to comment out this line in your ``sshd_config`` file as well (located at ``%PROGRAMDATA%\ssh``):

```
# Match Group administrators                                                    
#       AuthorizedKeysFile __PROGRAMDATA__/ssh/administrators_authorized_keys  
```

You will know if you were successful, because you will be able to execute the following command without having to be prompted for a password:
```
ssh [username]@[hostname]
```

Main Installation
-----------------

You can install it to any directory you want, but for this example I used ``C:\scripts\sftpshare``. Just copy the files from the ``\sftpshare\windows`` directory into the desired directory.

```
cp .\sftpshare\windows\* C:\scripts\sftpshare
```

Next, select a directory for placing files to be sent. You will drop off these files in this directory so they can be sent by the script. Let's assume that it will be ``C:\sftpshare\sending``. Edit the line in ``sftpshare.properties`` to reflect this.

```
# the staging directory for modified files in the sending directory.
sftpshare.files.out.staging.directory=C:\sftpshare\sending
```

Make sure that:

* The service account can read, write and delete files in the sending directory.
* The service account can execute the scripts.
* The service account can use SSH to connect to all the hosts.

You can then add the batch script sftpshare.bat to a schedule, such as on the Task Scheduler. Click on ``Action => Create Task`` and fill out the details.

Make sure that the service account is running the task if you are using Windows 10.

![Name: "run sftpshare". Description: "Runs the sftpshare client." Set the user account to sftpshare. Runs while the user is logged in or not is selected, do not store password, the task will only have access to local computer resources. Configure for Windows 10, and task is marked as hidden.](img/windows_install_ssh_07.png)

The initial trigger can be set for any time, but it will repeat every hour with this configuration.

![A trigger is being edited. It is set to repeat every hour, for a duration of indefinately. Is marked as enabled.](img/windows_install_ssh_08.png)

![A list of the triggers with new, edit and delete buttons.](img/windows_install_ssh_09.png)

You need to set the script, ``sftpshare.bat``, to be run.

![A list of actions that are executed when the task is run. The only action is "start a program" with the path set to C:\scripts\sftpshare\sftpshare.bat](img/windows_install_ssh_10.png)

Check whichever conditions you think should apply, however I marked the network connection as being required considering the nature of the service.

![A list of conditions on this task. All of them are the defaults except for "Start only if the following network connection is available" which is set to true and selected as "Any Connection".](img/windows_install_ssh_11.png)

If you have configured the service properly, it should trigger and loop every hour. If you are using non Pro Windows, you can only configure the trigger to run while you are logged in.

Optional Installation
---------------------

Since you are opening your computer to SSH connections, you should probably have some restrictions on who can just log in, as well as update other configurations such as the listening port number. This is by no means a comprehensive setup and assumes you're using openSSH. This requires you modifying your ``sshd_config`` file, which is located usually at ``%PROGRAMDATA%\ssh``.

An example of a restriction: Using ``AllowUsers`` to only allow SSH access to the service account:
```
AllowUsers sftpshare
```

You can read more about ``sshd_config`` [here](https://www.ssh.com/ssh/sshd_config/) and [here](https://man.openbsd.org/sshd_config).