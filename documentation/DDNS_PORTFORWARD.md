Setting Up DDNS and Port Forwarding
===================================

Logging into your router
------------------------

You can usually log in by finding the default gateway IP for your router:

Windows:
```
ipconfig
```
Found under "Default Gateway".


UNIX:
```
ip route show | grep default
```

Type the IP address in your browser address bar and you should be directed to the router's login page. Review your router's documentation for initial login and password information. If this information has been changed, contact a network administrator for assistance.

DDNS
----

DDNS stands for "Dynamic DNS", DNS being a service that routes a web address like ``example.com`` to a specific router by IP address. Because ISPs regularly change your IP address with a Dynamic IP Address, you will want to set up a DDNS so your connection string doesn't break when the dynamic IP updates.

You can get a free DNS from [FreeDNS](https://freedns.afraid.org/). After making an account, go to "Subdomains" and add a subdomain. After you have created the subdomain, go to "Dynamic DNS", copy the direct URL of the domain. The part after ``https://freedns.afraid.org/dynamic/update.php?`` is your update key. 

To add the new domain to your router, follow the instructions [here](https://freedns.afraid.org/guide/dd-wrt/). Alternatively you can also use an application that updates the DNS as well, if your router doesn't support DDNS.

Port Forwarding
---------------

Port Forwarding tells your router to point a specific port number to go to a specific device on your network.

First, you will need the LAN address to your device. LAN addresses usually start with ``192.168.*.*``. To get your LAN address, do the following:

Windows:

```
ipconfig
```
Found under "IPv4 Address".

UNIX:

```
hostname -I
```

Then go to your router settings. Usually Port Forwarding is under firewall settings but each type of router is different. Regardless, you will want to add a port forwarding entry for the SSH port (default 22) for type TCP to the IP address of your device.

Testing
-------

To test all of this, try the following from another computer with an SSH client:

```
ssh [username]@[Domain]
```

If it connects, congratulations, everything has been set up properly!