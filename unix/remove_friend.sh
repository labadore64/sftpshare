#!/bin/bash

## variables

NAME=$1
FILESOUT=`grep "sftpshare.files.out.staging.directory" ./sftpshare.properties | sed 's/sftpshare.files.out.staging.directory=//'`

if [ -s "friendlist" ]
then

	## get the name of the friend you want to remove

	while [ -z $NAME ]
	do
		echo "Which friend do you want to remove?"
		read -p "" NAME
		
		if [ -z $NAME ]
		then
			echo "You must input your friend's name!"
			echo ""		
			NAME=""
			continue
		fi

		TESTNAME=`grep -w $NAME friendlist`
		
		if [ -z "$TESTNAME" ]
		then
			echo "This friend isn't in your friend list!"
			echo ""		
			NAME=""
			continue
		fi
	done

	## removes your friend from the friend list

	LINENUMBER=`grep -w -n -m 1 $NAME friendlist | cut -f1 -d:`
	LASTLINE=$((LINENUMBER+2))
	SEDSTRING=$LINENUMBER","$LASTLINE"d"
	sed -i $SEDSTRING friendlist
else
	echo "Your friend list is already empty."
fi

