#!/bin/bash

FRIENDLIST=`cat ./friendlist | sed '/^[[:space:]]*$/d'`

IFS=$'\n' read -d '' -r -a lines  <<< "$FRIENDLIST"

## TestSFTP

## Tests whether or not you can connect with SFTP with a certain host
## Returns 1 if failed, 0 if passed.
function TestSFTP {
	HOST=$1

	## do the test
	
	echo "exit" > "tmp_batch"
	TEXT="$(sftp -b 'tmp_batch' $HOST)"
	rm "tmp_batch"

	## test for errors

	if [[ -z $TEXT ]]
	then
		return 1
	elif [[ $TEXT =~ "Connection timed out." ]]
	then
		return 1
	elif [[ $TEXT =~ "Connection closed" ]]
	then
		return 1
	elif [[ $TEXT =~ "No route to host" ]]
	then
		return 1
	fi

	return 0
}

# get length of an array
arraylength=${#lines[@]}

for (( i=0; i<${arraylength}-2; i+=3 ));
do
	echo "iteration " ${lines[$i]}
	NAME=${lines[$i]}
	HOST=${lines[$i+1]}
	DIRECTORY=${lines[$i+2]}

	TestSFTP $HOST

	TEST=$?
	
	if [ $TEST -eq 0 ]
	then
		echo "Connection Succeeded."
		./sftpshare.sh $NAME $HOST $DIRECTORY
	else
		echo "Connection unsuccessful. Skipping "$NAME"..."
	fi
done
