#!/bin/bash

## variables

NAME=`echo $1 | awk '{$1=$1};1'`
HOST=`echo $2 | awk '{$1=$1};1'`
DIRECTORY=`echo $3 | awk '{$1=$1};1'`
FILESOUT=`grep "sftpshare.files.out.staging.directory" ./sftpshare.properties | sed 's/sftpshare.files.out.staging.directory=//'`

DELETE_FILES=""

## functions

## SetUpBatch
## sets up the batch for this user
function SetUpBatch {
	DESTINATION=$1
	DIR=$2
	DELETELIST=$3
	
	# create an array with all the filer/dir inside ~/myDir
	arr=($DIR/*)

	# iterate through array using a counter
	for ((i=0; i<${#arr[@]}; i++)); do
		if [ -f "${arr[$i]}" ]
		then
			echo "put \"${arr[$i]}\"" >> ./tmp_batch
			if [ $DELETELIST -eq 1 ]
			then
				#add this entry to the delete list.
				DELETE_FILES=$DELETE_FILES"\"${arr[$i]}\","
			fi
		fi	
	done
}

## main script

echo "Transfering files for friend ${NAME}..."

# create the temp batch file
> ./tmp_batch

# set the first line of the batch file to cd to directory

echo "cd "$DIRECTORY >> ./tmp_batch

# Get all the global files.
# You don't want to delete any of the files here yet.

SetUpBatch $HOST "$FILESOUT" 0

# Get all the user specific files.
# You want to delete these after you send them.

SetUpBatch $HOST "${FILESOUT}/${NAME}" 1

# Executes batch

sftp -b "tmp_batch" $HOST

# Deletes files specific to the user

IFS=$',' read -d '' -r -a deletefiles  <<< "$DELETE_FILES"

arraylength=${#deletefiles[@]}

for (( i=0; i<${arraylength}-2; i+=3 ));
do
	rm -f "${deletefiles[$i]}"
done

# when you're done remove the temp batch file
rm ./tmp_batch
