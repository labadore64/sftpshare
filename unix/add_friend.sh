#!/bin/bash

## variables

NAME=$1
HOST=$2
DIRECTORY=$3
FILESOUT=`grep "sftpshare.files.out.staging.directory" ./sftpshare.properties | sed 's/sftpshare.files.out.staging.directory=//'`

## get name

while [ -z $NAME ]
do
	echo "What is your friend's name?"
	read -p "" NAME
	if [ -z $NAME ]
	then
		echo "You must input your friend's name!"
		echo ""
		NAME=""
		continue
	fi

	TESTNAME=`grep -w $NAME friendlist`
	
	if [ ! -z "$TESTNAME" ]
	then
		echo "This friend already exists!"
		echo ""		
		NAME=""
		continue
	fi
done

## get host

while [ -z $HOST ]
do
	echo "What is your friend's connection string?"
	read -p "" HOST
	if [ -z $HOST ]
	then
		echo "You must input your friend's connection string!"
		echo ""
		continue
	fi

	TESTNAME=`grep -w $HOST friendlist`
	
	if [ ! -z "$TESTNAME" ]
	then
		echo "This connection string already exists for someone else!"
		echo ""		
		HOST=""
		continue
	fi
done

# get receiving directory

while [ -z $DIRECTORY ]
do
	echo "What is the directory on your friend's computer where the files should be transfered?"
	read -p "" DIRECTORY
	if [ -z $DIRECTORY ]
	then
		echo "You must put in your friend's receiving directory!"
		echo ""
		DIRECTORY=""
		continue
	fi
done

NEWDIR=$FILESOUT"/"$NAME
if [ -d $NEWDIR ]
then
	echo ""
	echo "Path for this friend already exists. No need to create it."
	echo ""
else
	echo ""
	echo "Creating path for "$NAME": "$NEWDIR
	mkdir $NEWDIR
	echo ""
fi

echo $NAME >> ./friendlist
echo $HOST >> ./friendlist
echo $DIRECTORY >> ./friendlist
echo "" >> ./friendlist

echo "Congratulations! You added your friend to the transfer list."
echo "Please keep in mind!"
echo "You must exchange SSH tokens with your friend in order for this script to function automatically."
echo ""
