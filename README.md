sftpshare: batch sftp scripts
===========

sftpshare is a set of bash and powershell scripts that automates sftp file sharing with your friends by managing a friend list. It can be installed to automatically send files with a crontab or scheduler. Please view the following topics to help installation and use of the scripts.

Installation
------------

* [Windows 10](documentation/INSTALL_WIN.md)
* [Linux](documentation/INSTALL_LINUX.md)

Usage
-----------

* [Setting Up DDNS and Port Forwarding](documentation/DDNS_PORTFORWARD.md)
* [File Management](documentation/FILE_MANAGE.md)
* [Friend List Management](documentation/FRIEND.md)
* [Scripts](documentation/SCRIPTS.md)